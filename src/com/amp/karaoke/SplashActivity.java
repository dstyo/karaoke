package com.amp.karaoke;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class SplashActivity extends Activity{
	private static final int SPLASH_TIME = 2 * 1000;// 3 seconds

    protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        openStrictModeByVersion();

        try {
            new Handler().postDelayed(new Runnable() {
                public void run() {
                        success();
                }


            }, SPLASH_TIME);

            new Handler().postDelayed(new Runnable() {
                public void run() {
                }
            }, SPLASH_TIME);
        } catch(Exception e){}





	}

   
	@Override
	public void onBackPressed() {
		this.finish();
		super.onBackPressed();
	}

    private void openStrictModeByVersion()
    {
        float fv = 3.0f;

        if (fv > 2.3)
        {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                    .detectDiskReads()
                    .detectDiskWrites()
                    .detectNetwork()
                    .penaltyLog()
                    .build());
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectLeakedSqlLiteObjects()
                    .penaltyLog()
                    .penaltyDeath().build());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void success() {
        final Intent intent = new Intent(getApplicationContext(),
                MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        SplashActivity.this.finish();
                        startActivity(intent);
    }

    



}
