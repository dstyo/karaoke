package com.amp.karaoke;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.amp.karaoke.adapter.CustomListAdapter;
import com.amp.karaoke.app.AppController;
import com.amp.karaoke.app.IpAddress;
import com.amp.karaoke.model.SongList;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.database.DataSetObserver;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore.Audio;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class HomeFragmentMain extends Fragment {
	
	private String jsonSongList="sl_11577224";
	private String jsonImage="download/";

	ListView LVSong;
	
	private static final String TAG_SONGLIST = "list";
	private static final String TAG_TITLE = "title";
	private static final String TAG_ARTIST = "artist";
	private ProgressDialog pDialog;

	JSONArray song_list = null;
	private List<SongList> movieList = new ArrayList<SongList>();
	public HomeFragmentMain(){}
	CustomListAdapter adapter;
	private  MediaPlayer mediaplayer;
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_homemain, container, false);
        LVSong = (ListView) rootView.findViewById(R.id.lvListSong);
      
        //adapter = new CustomListAdapter(this, movieList);
	     //   LVSong.setAdapter(adapter);
        new GetListSongs().execute();


		mediaplayer = new MediaPlayer();
		mediaplayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        
        return rootView;
    }
	
	private class GetListSongs extends AsyncTask<Void, Void, Void> {
		 
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
              pDialog = new ProgressDialog(getActivity());
              pDialog.setMessage("Please wait...");
              pDialog.setCancelable(false);
              pDialog.show();
 
        }
 
        @Override
        protected Void doInBackground(Void... arg0) {
        	getActivity().runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					JsonObjectRequest movieReq = new JsonObjectRequest(IpAddress.host+jsonSongList,null,
		    				new Response.Listener<JSONObject>() {
		    					@Override
		    					public void onResponse(JSONObject response) {
		    						
		    					//	Log.d(TAG, response.toString());
		    						hidePDialog();
		    						JSONObject song_list = null;
		    						JSONArray artist = null;
		    						try {
		    							song_list = response.getJSONObject("list");
		    						} catch (JSONException e1) {
		    							// TODO Auto-generated catch block
		    							e1.printStackTrace();
		    						}
		    						
		    						try {
		    							artist = song_list.getJSONArray("artist");
		    							System.out.println(artist);
		    						} catch (JSONException e1) {
		    							// TODO Auto-generated catch block
		    							e1.printStackTrace();
		    						}
		    						// Parsing json
		    						for (int i = 0; i < artist.length(); i++) {
		    							try {
		    								JSONObject obj = artist.getJSONObject(i);
		    								SongList songlist = new SongList();
		    								songlist.setTitle(obj.getString("artist"));
		    								songlist.setThumbnailUrl(IpAddress.host+jsonImage+obj.getString("picture"));
		    								songlist.setRating(2);
		    								songlist.setYear(2010);
		    								ArrayList<String> genre = new ArrayList<String>();
		    								//genre.add("genre");
		    								songlist.setGenre(genre);
		    							/*	movie.setThumbnailUrl(obj.getString("image"));
		    								movie.setRating(((Number) obj.get("rating"))
		    										.doubleValue());
		    								movie.setYear(obj.getInt("releaseYear"));

		    								// Genre is json array
		    								JSONArray genreArry = obj.getJSONArray("genre");
		    								ArrayList<String> genre = new ArrayList<String>();
		    								for (int j = 0; j < genreArry.length(); j++) {
		    									genre.add((String) genreArry.get(j));
		    								}
		    								movie.setGenre(genre);*/

		    								// adding movie to movies array
		    								movieList.add(songlist);

		    							} catch (JSONException e) {
		    								e.printStackTrace();
		    							}

		    						}
		    						
		    						adapter = new CustomListAdapter(getActivity(), movieList);
		    			            LVSong.setAdapter(adapter);
		    						// notifying list adapter about data changes
		    						// so that it renders the list view with updated data
		    			            
		    			           
										LVSong.setOnItemClickListener(new AdapterView.OnItemClickListener() {

											@Override
											public void onItemClick(
													AdapterView<?> arg0,
													View arg1, int arg2,
													long arg3) {
												// TODO Auto-generated method stub
												try {
													mediaplayer.setDataSource("http://192.168.1.100:8000/dewata/default/download/t_song_list.f_preview.86785a90a0e53236.7477696e676b6c655f707265766965772e6d7033.mp3");
												} catch (IllegalArgumentException e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												} catch (SecurityException e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												} catch (IllegalStateException e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												} catch (IOException e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}
												try {
													mediaplayer.prepare();
												} catch (IllegalStateException e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												} catch (IOException e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}
												mediaplayer.start();
											}
										});
									
		    						
		    					}
		    				}, new Response.ErrorListener() {
		    					@Override
		    					public void onErrorResponse(VolleyError error) {
		    					//	VolleyLog.d(TAG, "Error: " + error.getMessage());
		    						//hidePDialog();

		    					}
		    				});

		    		// Adding request to request queue
		    		AppController.getInstance().addToRequestQueue(movieReq);
		            
				}
			});
			return null;
        	
        }
 
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
           
            
            
        }
 
    }
	
	private void hidePDialog() {
		if (pDialog != null) {
			pDialog.dismiss();
			pDialog = null;
		}
	}
	
}
	