package com.amp.karaoke;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.amp.karaoke.adapter.CustomListAdapter;
import com.amp.karaoke.app.AppController;
import com.amp.karaoke.app.IpAddress;
import com.amp.karaoke.model.SongList;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class HomeFragment extends Fragment {
	
	private static final String TAG = MainActivity.class.getSimpleName();

	
	private String jsonSongList="sl_11577224";
	private String jsonImage="download/";
	private ProgressDialog pDialog;
	private List<SongList> movieList = new ArrayList<SongList>();
	private ListView LVSong,LVGenre,LVCategory;
	private CustomListAdapter adapter;
	ArrayList<HashMap<String, String>> songArrayList,genreList,categoryList;

	
	public HomeFragment(){}
	
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        LVSong = (ListView) rootView.findViewById(R.id.LvListArtist);
        LVGenre = (ListView) rootView.findViewById(R.id.lvListGenre);
        LVCategory= (ListView) rootView.findViewById(R.id.lvListCategories);
       // songList = new ArrayList<HashMap<String, String>>();
        genreList = new ArrayList<HashMap<String, String>>();
        categoryList = new ArrayList<HashMap<String, String>>();
        new GetListSongs().execute();
        return rootView;
    }
	
	
	/**
     * Async task class to get json by making HTTP call
     * */
    private class GetListSongs extends AsyncTask<Void, Void, Void> {
 
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
              pDialog = new ProgressDialog(getActivity());
              pDialog.setMessage("Please wait...");
              pDialog.setCancelable(false);
              pDialog.show();
 
        }
 
        @Override
        protected Void doInBackground(Void... arg0) {
        	getActivity().runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					JsonObjectRequest movieReq = new JsonObjectRequest(IpAddress.host+jsonSongList,null,
		    				new Response.Listener<JSONObject>() {
		    					@Override
		    					public void onResponse(JSONObject response) {
		    						
		    						Log.d(TAG, response.toString());
		    						hidePDialog();
		    						JSONObject song_list = null;
		    						JSONArray artist = null;
		    						JSONArray genre_list = null;
		    						JSONArray category_list = null;
		    						try {
		    							song_list = response.getJSONObject("list");
		    						} catch (JSONException e1) {
		    							// TODO Auto-generated catch block
		    							e1.printStackTrace();
		    						}
		    						
		    						try {
		    							genre_list = song_list.getJSONArray("category");
		    						} catch (JSONException e1) {
		    							// TODO Auto-generated catch block
		    							e1.printStackTrace();
		    						}
		    						
		    						for (int i = 0; i < genre_list.length(); i++) {
		    	                        
										try {
											JSONObject c = genre_list.getJSONObject(i);
											String id = c.getString("id");
			    	                        String name_genre = c.getString("category");
			    	                        HashMap<String, String> genre_list_HashMap = new HashMap<String, String>();
			    	                        genre_list_HashMap.put("id", id);
			    	                        genre_list_HashMap.put("category", name_genre);
			    	                        categoryList.add(genre_list_HashMap);
										} catch (JSONException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
		    	                        
		    						}
		    						
		    						try {
		    							genre_list = song_list.getJSONArray("genre");
		    							System.out.println(genre_list);
		    						} catch (JSONException e1) {
		    							// TODO Auto-generated catch block
		    							e1.printStackTrace();
		    						}
		    						
		    						for (int i = 0; i < genre_list.length(); i++) {
		    	                        
										try {
											JSONObject c = genre_list.getJSONObject(i);
											String id = c.getString("id");
			    	                        String name_genre = c.getString("genre");
			    	                        HashMap<String, String> genre_list_HashMap = new HashMap<String, String>();
			    	                        genre_list_HashMap.put("id", id);
			    	                        genre_list_HashMap.put("genre", name_genre);
			    	                        genreList.add(genre_list_HashMap);
										} catch (JSONException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
		    	                        
		    						}
		    						
		    						try {
		    							artist = song_list.getJSONArray("artist");
		    							System.out.println(artist);
		    						} catch (JSONException e1) {
		    							// TODO Auto-generated catch block
		    							e1.printStackTrace();
		    						}
		    						// Parsing json
		    						for (int i = 0; i < artist.length(); i++) {
		    							try {
		    								JSONObject obj = artist.getJSONObject(i);
		    								SongList songlist = new SongList();
		    								songlist.setTitle(obj.getString("artist"));
		    								songlist.setThumbnailUrl(IpAddress.host+jsonImage+obj.getString("picture"));
		    								songlist.setRating(2);
		    								songlist.setYear(2010);
		    								ArrayList<String> genre = new ArrayList<String>();
		    								//genre.add("genre");
		    								songlist.setGenre(genre);
		    							/*	movie.setThumbnailUrl(obj.getString("image"));
		    								movie.setRating(((Number) obj.get("rating"))
		    										.doubleValue());
		    								movie.setYear(obj.getInt("releaseYear"));

		    								// Genre is json array
		    								JSONArray genreArry = obj.getJSONArray("genre");
		    								ArrayList<String> genre = new ArrayList<String>();
		    								for (int j = 0; j < genreArry.length(); j++) {
		    									genre.add((String) genreArry.get(j));
		    								}
		    								movie.setGenre(genre);*/

		    								// adding movie to movies array
		    								movieList.add(songlist);

		    							} catch (JSONException e) {
		    								e.printStackTrace();
		    							}

		    						}
		    						 ListAdapter genre_adapter = new SimpleAdapter(
		    				                    getActivity(), genreList,
		    				                        R.layout.list_row_genre, new String[] {"id", "genre"}, new int[] { R.id.id_genre,
		    				                                R.id.name_genre});
		    				            
		    				         LVGenre.setAdapter(genre_adapter);
		    				         LVGenre.setOnItemClickListener(new AdapterView.OnItemClickListener() {

										@Override
										public void onItemClick(
												AdapterView<?> arg0, View arg1,
												int arg2, long arg3) {
											// TODO Auto-generated method stub
											Fragment fragment = null;
												fragment = new HomeFragmentMain();
											
											if (fragment != null) {
												FragmentManager fragmentManager = getFragmentManager();
												fragmentManager.beginTransaction()
														.replace(R.id.frame_container, fragment).commit();

												// update selected item and title, then close the drawer
											//	mDrawerList.setItemChecked(position, true);
											//	mDrawerList.setSelection(position);
											//	setTitle(navMenuTitles[position]);
											//	mDrawerLayout.closeDrawer(mDrawerList);
											} else {
												// error in creating fragment
												Log.e("MainActivity", "Error in creating fragment");
											}

										}
		    				        	 
									});
		    				         
		    				         ListAdapter category_adapter = new SimpleAdapter(
		 				                    getActivity(), categoryList,
		 				                        R.layout.list_row_genre, new String[] {"id", "category"}, new int[] { R.id.id_genre,
		 				                                R.id.name_genre});
		 				            
		 				             LVCategory.setAdapter(category_adapter);
		 				            
		 				           
		 				             
		 				         
		    					//	adapter.notifyDataSetChanged();
		    						adapter = new CustomListAdapter(getActivity(), movieList);
		    			            LVSong.setAdapter(adapter);
		    						// notifying list adapter about data changes
		    						// so that it renders the list view with updated data
		    						
		    					}
		    				}, new Response.ErrorListener() {
		    					@Override
		    					public void onErrorResponse(VolleyError error) {
		    						VolleyLog.d(TAG, "Error: " + error.getMessage());
		    						//hidePDialog();

		    					}
		    				});

		    		// Adding request to request queue
		    		AppController.getInstance().addToRequestQueue(movieReq);
		            
				}
			});
			return null;
        	
        }
 
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
           
            
            
        }
 
    }
    
    private void hidePDialog() {
		if (pDialog != null) {
			pDialog.dismiss();
			pDialog = null;
		}
	}
}
